package controller.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.fintech.Receitas;

public class ReceitasDao {
	public void create(Receitas receita) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;

		try {
			stmt = con.prepareStatement(
					"INSERT INTO T_SFF_RECEITA" + " (COD_USUARIO,COD_CATEGORIA_RECEITA,NOM_RECEITA,DS_VALOR,DS_DATA)"
							+ " VALUES (SQ_RECEITAS.NEXTVAL,?,?,?,?) ");

			stmt.setInt(1, receita.getCOD_CATEGORIA_RECEITA());
			stmt.setString(2, receita.getNOM_RECEITA());
			stmt.setDouble(3, receita.getDS_VALOR());
			stmt.setString(4, receita.getDS_DATA());

			stmt.executeUpdate();
		} catch (SQLException e) {
			
			Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE,null,e);
		}

	}

	public List<Receitas> getAll() {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		List<Receitas> lista = new ArrayList<Receitas>();
		ResultSet result = null;
		try {
			stmt = con.prepareStatement("SELECT * FROM T_SFF_RECEITA");
			result = stmt.executeQuery();

			while (result.next()) {
				Receitas receita = new Receitas();
				receita.setCOD_RECEITA(result.getInt("COD_RECEITA"));
				receita.setCOD_CATEGORIA_RECEITA(result.getInt("COD_CATEGORIA_RECEITA"));
				receita.setDS_VALOR(result.getDouble("DS_VALOR"));
				receita.setNOM_RECEITA(result.getString("NOM_RECEITA"));
				receita.setDS_DATA(result.getString("DS_DATA"));
				lista.add(receita);

			}
			return lista;
		} catch (SQLException e) {
			throw new RuntimeException("Erro ao consultar a base de dados", e);
		} finally {

			ConnectionFactory.closeConnection(con, stmt, result);

		}
	}
}
