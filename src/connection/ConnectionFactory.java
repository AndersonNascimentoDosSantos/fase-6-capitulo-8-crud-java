package connection;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;

public class ConnectionFactory {
	private static final String DRIVER = "oracle.jdbc.driver.OracleDriver"; 
	private static final String HOST ="jdbc:oracle:thin:@oracle.fiap.com.br:1521:ORCL";
	private static final String USER = "RM89390";
	private static final String PASSWORD = "310192";
	
	public ConnectionFactory() {}

	public static  Connection getConnection() {
		
		try {		
			
			Class.forName(DRIVER);
			
			return DriverManager.getConnection(HOST,USER,PASSWORD);
			
			
		}catch(SQLException e){
			throw new RuntimeException("Não foi possível conectar no Banco de Dados",e);
			
			
		}catch(ClassNotFoundException e) {
			
			throw new RuntimeException("O Driver JDBC não foi encontrado!",e);



			
			
		}
		
	
	}

	
	public static void closeConnection(Connection con) {
		
			try {
				if(con!= null) {
				con.close();
				}
			} catch (SQLException e) {
				
				Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE,null,e);
			}
		
	}
	public static void closeConnection(Connection con,PreparedStatement stmt) {
		closeConnection(con);
		try {
			if(stmt!=null) {
				stmt.close();
			}
			
		} catch (SQLException e) {
		
			Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE,null,e);
		}
	
}
	public static void closeConnection(Connection con,PreparedStatement stmt,ResultSet rs) {
		closeConnection(con,stmt);
		try {
			if(rs!=null) {
				rs.close();
			}
			
		} catch (SQLException e) {
			
			Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE,null,e);
		}
	
}
}

