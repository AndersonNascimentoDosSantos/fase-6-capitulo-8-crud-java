package model.fintech;

/**
 * @author anderson
 *
 */
public class Receitas {
	private int COD_RECEITA;
	private int COD_CATEGORIA_RECEITA;
	private String NOM_RECEITA;
	private Double DS_VALOR;
	private String DS_DATA;
	
	public int getCOD_CATEGORIA_RECEITA() {
		return this.COD_CATEGORIA_RECEITA;
	}
	public void setCOD_CATEGORIA_RECEITA(int cOD_CATEGORIA_RECEITA) {
		this.COD_CATEGORIA_RECEITA = cOD_CATEGORIA_RECEITA;
	}
	public String getNOM_RECEITA() {
		return this.NOM_RECEITA;
	}
	public void setNOM_RECEITA(String nOM_RECEITA) {
		this.NOM_RECEITA = nOM_RECEITA;
	}
	public String getDS_DATA() {
		return this.DS_DATA;
	}
	public void setDS_DATA(String dS_DATA) {
		this.DS_DATA = dS_DATA;
	}
	public Double getDS_VALOR() {
		return this.DS_VALOR;
	}
	public void setDS_VALOR(Double dS_VALOR) {
		this.DS_VALOR = dS_VALOR;
	}
	public void setCOD_RECEITA(int cod_RECEITA) {		
		this.COD_RECEITA = cod_RECEITA;				
	}
	
	public int getCOD_RECEITA() {
		return this.COD_RECEITA;
	}
	
	
}
